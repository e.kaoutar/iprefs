let express = require('express')
let app = express()
let bodyParser = require('body-parser')

//using view folder for all ejs files
app.set('view engine', 'ejs')

//all static files are saved in assets path
app.use('/assets', express.static('public'))

//Middleware
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


app.get('/', (request, response) => {
    response.render('index')
})


//verifier l'ID
app.post('/submitId', (request, response) => {
    
    if(request.body.idEmployee === undefined || request.body.idEmployee === '')
    {
        response.render('index', {error: 'ID manquant ou incorrect réessayez à nouveau'})
    }
    else{
        response.render('preferences')
    }
})

app.post('/submitpreferences', (request, response)=>{
    
    let heighparam = request.body.selectedHeight;
    let mouseparam = request.body.selectmouse;
    let shapeparam = request.body.selectedShape;
    let lighteningparam = request.body.selectedLight;

    response.render('choosenpeferences', {heigh: heighparam, mouse: mouseparam, shape: shapeparam, light: lighteningparam})

})

app.get('/preferences', (request, response)=>{
    response.render('preferences');
})

//listening on port 8080
app.listen(8080)